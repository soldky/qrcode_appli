import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

/*
  Generated class for the HistoryProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HistoryProvider {

  constructor(public http: HttpClient, public storage: Storage) {}

  public save(value) {
    let history: any;

    this.get("history").then((val) => {
      history = val;
      if(!history) {
        history = [];
      }
      history.push({
          "date": new Date(),
          "value": value
      });
      this.set("history", history);
    });
  }

  public set(key, value) {
    this.storage.set(key, value);
  }

  public get(key) {
    return this.storage.get(key);
  }

}
