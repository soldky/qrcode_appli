import { Component } from '@angular/core';
import { QrCodeProvider } from '../../providers/qr-code/qr-code';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';

/**
 * Generated class for the ScannerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-scanner',
  templateUrl: 'scanner.html',
})
export class ScannerPage {

  protected value: string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams, public qrcode: QrCodeProvider, public camera: Camera) {}

  public scan() {
    this.qrcode.scan().then((data) => {
      this.value = data.text;
    });
  }

  public scanByPicture() {
    let cameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.FILE_URI,
      quality: 100,
      targetWidth: 1000,
      targetHeight: 1000,
      encodingType: this.camera.EncodingType.JPEG,
      correctOrientation: true
    }
    this.camera.getPicture(cameraOptions).then((file_uri) => {
      this.value = file_uri;
    });
  }

}
