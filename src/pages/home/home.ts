import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { QrCodeProvider } from '../../providers/qr-code/qr-code';
import { HistoryProvider } from '../../providers/history/history';
import { SocialSharing } from '@ionic-native/social-sharing';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  protected text: string = "";
  protected qrcodeUrl: string = "";

  constructor(public navCtrl: NavController, public qrcode: QrCodeProvider, public history: HistoryProvider, private socialSharing: SocialSharing) {}

  public generate() {
    this.qrcode.generate(this.text).then((url) => {
      this.qrcodeUrl = url;
      this.history.save(this.text);
    });
  }

  public share() {
    this.socialSharing.share(this.text, null, this.qrcodeUrl, null).then(() => {

    });
  }

}
