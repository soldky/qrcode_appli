import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HistoryProvider } from '../../providers/history/history';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {

  protected history: Object[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public historyProvider: HistoryProvider) {
    this.historyProvider.get("history").then((val) => {
      this.history = val;
      this.history.sort(this.sort);
    });
  }

  public sort(a, b) {
    if(a.date > b.date) {
      return -1;
    }
    else if(a.date < b.date) {
      return 1;
    }
    return 0;
  }

}
